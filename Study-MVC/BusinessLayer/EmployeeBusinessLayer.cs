﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Study_MVC.Models;
using Study_MVC.DataAccessLayer;

namespace Study_MVC.BusinessLayer {
    public class EmployeeBusinessLayer {
        public List<Employee> GetEmployees() {
            var salesDal = new SalesERPDAL();
            return salesDal.Employees.ToList();
        }

        public Employee SaveEmployee(Employee e) {
            SalesERPDAL salesDal = new SalesERPDAL();
            salesDal.Employees.Add(e);
            salesDal.SaveChanges();
            return e;
        }

        public bool IsValidUser(UserDetails u)
        {
            if (u.UserName == "Admin" && u.Password == "Admin")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}